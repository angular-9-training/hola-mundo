import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.sass']
})
export class PersonaComponent implements OnInit {

  @Input() name: string = 'Adrián';
  @Output() mensaje = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
    console.log(this.name);
  }

  pulsarBoton(): void {
    //alert('El botón ha sido pulsado');
    this.mensaje.emit('Hola desde el componente hijo!');
  }

}
