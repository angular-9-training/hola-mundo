import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Para usar NgModel (Two-way Data Binding)
import { FormsModule , ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PersonaComponent } from './components/persona/persona.component';
import { AnimalComponent } from './components/animal/animal.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonaComponent,
    AnimalComponent
  ],
  imports: [
    BrowserModule,
    //Para usar NgModel (Two-way Data Binding)
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
