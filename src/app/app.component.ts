import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'hola-mundo';
  name: string = 'Adrián';

  mensaje: string = 'Mensaje for defecto';

  obtenerMensaje(mensaje: string) {
    this.mensaje = mensaje;
  }

}
